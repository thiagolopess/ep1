#ifndef CANOA_HPP
#define CANOA_HPP
#include "embarcacao.hpp"

using namespace std;

class Canoa: public Embarcacao{

    public:
        Canoa();
        ~Canoa();

        bool destroi_canoa();

};

#endif