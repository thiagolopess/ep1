#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <string>

using namespace std;

class Embarcacao{

    private:
        string tipo;
        char id;
        int tamanho;

    public:
        Embarcacao();
        ~Embarcacao();

        char get_id();
        void set_id(char id);

        string get_tipo();
        void set_tipo(string tipo);

        int get_tamanho();
        void set_tamanho (int tamanho);
};

#endif