#include "../inc/embarcacao.hpp"
#include <string>

Embarcacao::Embarcacao(){}

Embarcacao::~Embarcacao(){}

string Embarcacao::get_tipo(){
    return tipo;
}
void Embarcacao::set_tipo(string tipo){
    this->tipo = tipo;
}
int Embarcacao::get_tamanho(){
    return tamanho;
}
void Embarcacao::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}
char Embarcacao::get_id(){
    return id;
}
void Embarcacao::set_id(char id){
    this->id = id;
}