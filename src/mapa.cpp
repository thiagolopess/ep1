#include <iostream>
#include <string>
#include <fstream>
#include "../inc/mapa.hpp"
#include "../inc/submarino.hpp"
#include "../inc/portaAvioes.hpp"
#include "../inc/canoa.hpp"

using namespace std;

    Mapa::Mapa(){
        set_tamanho(13);
    }
    Mapa::~Mapa(){}

    int Mapa::get_tamanho(){
        return tamanho;
    }
    void Mapa::set_tamanho(int tamanho){
        this->tamanho = tamanho;
    }

    void Mapa::cria_mapa(char tabuleirop1[13][13], char tabuleirop2[13][13]){
        for (int i = 0; i < 13; i++){
            for (int j = 0; j < 13; j++){
                tabuleirop1[i][j] = ' ';
                tabuleirop2[i][j] = ' ';
            }
        }

    }

    Submarino submarinos;
    PortaAvioes porta_avioes;
    Canoa canoas;

    
    void Mapa::le_mapa(vector <pair <int, pair <int, string>>> *vector1, vector <pair <int, pair <int, string>>> *vector2){
        string linha = "";
        int i = 0, size = 0, contador_espaco = 0;
        string x_string, y_string, tipo, direcao; //Armazena as informações do arquivo
        int x = 0, y = 0; // Para converter as strings
        ifstream file;
        file.open("./doc/map_1.txt.txt");

        while(getline(file, linha)){
            
            if(linha.empty() || (linha[0] == '#' && (linha != "# player_1" && linha != "# player_2"))){
                continue;
            }
            if (linha == "# player_1"){
                getline(file, linha);
                getline(file, linha);
                while (linha != "# player_2"){
                    if (linha != ""){ 
                        size = linha.size();
                        for (i = 0; i < size; i++){
                            if (linha[i] != ' '){
                                if(contador_espaco == 0){
                                        x_string.push_back(linha[i]);
                                }else if(contador_espaco == 1){
                                        y_string.push_back(linha[i]);
                                }else if(contador_espaco == 2){
                                        tipo.push_back(linha[i]);
                                }else if(contador_espaco == 3){
                                        direcao.push_back(linha[i]);
                                }
                            }else{
                                contador_espaco++;
                            }  
                        }  
                        x = stoi(x_string);
                        y = stoi(y_string);
                        if(tipo == "canoa"){
                           vector1->push_back(make_pair(x, make_pair(y, tipo)));
                        }else if(tipo == "submarino"){
                            if(direcao == "direita"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                        vector1->push_back(make_pair(x, make_pair(y++, tipo)));
                                }
                            }else if(direcao == "esquerda"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector1->push_back(make_pair(x, make_pair(y--, tipo)));
                                }
                            }else if(direcao == "cima"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector1->push_back(make_pair(x--, make_pair(y, tipo)));
                                }
                            }
                            else if(direcao == "baixo"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector1->push_back(make_pair(x++, make_pair(y, tipo)));
                                }
                            }
                        }else if(tipo == "porta-avioes"){
                            if(direcao == "direita"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector1->push_back(make_pair(x, make_pair(y++, tipo)));
                                }
                            }else if(direcao == "esquerda"){
                                    for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                       vector1->push_back(make_pair(x, make_pair(y--, tipo)));
                                }
                            }
                            else if(direcao == "cima"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector1->push_back(make_pair(x--, make_pair(y, tipo)));
                                }
                            }
                            else if(direcao == "baixo"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector1->push_back(make_pair(x++, make_pair(y, tipo)));
                                }
                            }
                        } 
                            x_string = ""; y_string = ""; tipo = ""; direcao = "";
                            contador_espaco = 0;     
                    }  
                    getline(file, linha);
                }
            }    
            if (linha == "# player_2"){
                getline(file, linha);
                contador_espaco = 0;
                while(getline(file, linha)){ 
                    if(linha != ""){
                        size = linha.size();
                        for (i = 0; i < size; i++){
                            if (linha[i] != ' '){
                                if(contador_espaco == 0){
                                        x_string.push_back(linha[i]);
                                }else if(contador_espaco == 1){
                                        y_string.push_back(linha[i]);
                                }else if(contador_espaco == 2){
                                        tipo.push_back(linha[i]);
                                }else if(contador_espaco == 3){
                                        direcao.push_back(linha[i]);
                                }
                            }else{
                                contador_espaco++;
                            }  
                        }  
                        x = stoi(x_string);
                        y = stoi(y_string);
                        if(tipo == "canoa"){
                           vector2->push_back(make_pair(x, make_pair(y, tipo)));
                        }else if(tipo == "submarino"){
                            if(direcao == "direita"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                        vector2->push_back(make_pair(x, make_pair(y++, tipo)));
                                }
                            }else if(direcao == "esquerda"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector2->push_back(make_pair(x, make_pair(y--, tipo)));
                                }
                            }else if(direcao == "cima"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector2->push_back(make_pair(x--, make_pair(y, tipo)));
                                }
                            }
                            else if(direcao == "baixo"){
                                for(int i = 0;i<submarinos.get_tamanho();i++){
                                    vector2->push_back(make_pair(x++, make_pair(y, tipo)));
                                }
                            }
                        }else if(tipo == "porta-avioes"){
                            if(direcao == "direita"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector2->push_back(make_pair(x, make_pair(y++, tipo)));
                                }
                            }else if(direcao == "esquerda"){
                                    for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                       vector2->push_back(make_pair(x, make_pair(y--, tipo)));
                                }
                            }
                            else if(direcao == "cima"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector2->push_back(make_pair(x--, make_pair(y, tipo)));
                                }
                            }
                            else if(direcao == "baixo"){
                                for(int i = 0;i<porta_avioes.get_tamanho();i++){
                                    vector2->push_back(make_pair(x++, make_pair(y, tipo)));
                                }
                            }
                        }   
                            x_string = ""; y_string = "";  tipo = ""; direcao = "";
                            contador_espaco = 0;      
                    } 
                }         
            }
        }                   
    }          